Feature('1-(AdminSite) Category');

Scenario('1.1.1 Add category', (I,Admin)=>{
	Admin.login()
	Admin.newCategory()
	Admin.parentCategory('NIKE')
	Admin.saveCategory()
})
Scenario('1.1.2 Add Category with Image',(I,Admin)=>{
	Admin.login()
	Admin.newCategoryWithImage()
})
Scenario('1.2.1,.5 Edit Category and Assign as Special', (I,Admin)=>{
	Admin.login()
	Admin.editCategory()
	Admin.specialCategory('Sale')
	Admin.saveCategory()
})
Scenario('1.2.2 Edit Parent Category', (I,Admin)=>{
	Admin.login()
	Admin.editParentCategory('Asics Onitsuka')
	Admin.saveCategory()
})
Scenario('1.2.3 Publish/Unpublish Category', (I,Admin)=>{
	Admin.login()
	Admin.categoryPublish()
	Admin.saveCategory()
})
Scenario('1.3.1 Reorder Category',(I,Admin)=>{
	Admin.login()
	Admin.reorderCategory()
})
Scenario('1.4.1 Search Category',(I,Admin)=>{
	Admin.login()
	Admin.search('1')
})
Scenario('1.5.1 Remove Category', (I,Admin)=>{
	Admin.login()
	Admin.deleteCategory()
})