Feature('3-(AdminSite) Orders');

Scenario('3.1.1 Change Order to Processing', (I,Admin)=>{
	Admin.login()
	Admin.changeOrderStatusToProcessing("201900016")
})
Scenario('3.1.2 Change Order Status to Shipped', (I,Admin)=>{
	Admin.login()
	Admin.changeOrderStatusToShipped("201900016")
})
Scenario('3.1.3 Change Order Status to Cancelled',(I,Admin)=>{
	Admin.login()
	Admin.changeOrderStatusToCancelled("201900016")
})
Scenario('3.2.1 Export Order', (I,Admin)=>{
	Admin.login()
	Admin.exportOrder()
})
Scenario('3.3 Select Report Option',(I,Admin)=>{
	Admin.login()
	Admin.selectReportOption('1')
})
Scenario('3.4.1 Filter Order',(I,Admin)=>{
	Admin.login()
	Admin.filterOrders('order status','Cancelled')
})
Scenario('3.5 Delete Order',(I,Admin)=>{
	Admin.login()
	Admin.deleteOrder("201900021")
})
