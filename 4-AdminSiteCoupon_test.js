Feature('4-(AdminSite) Coupon');

Scenario('4.1.1 Add Product Coupon',(I,Admin,Kupon)=>{
	Admin.login()
	Kupon.addProductCoupon()
	Kupon.couponCode('AUTOTEST','Kupon Product Automation')
	Kupon.setDiscountRate('50')
	Kupon.isValidForAllProducts()
	Kupon.saveCoupon()
})
Scenario('4.1.2 Add Cart Coupon',(I,Admin,Kupon)=>{
	Admin.login()
	Kupon.addCartCoupon()
	Kupon.couponCode('CARTAUTO','Kupon Cart Automation')
	Kupon.setDiscountAmount('20000')
	Kupon.setShippingDiscount('20')
	Kupon.saveCoupon()
})
Scenario('4.2 Edit Product Coupon',(I,Admin,Kupon)=>{
	Admin.login()
	Kupon.editProductCoupon('AUTOTEST')
})
Scenario('4.3 Edit Cart Coupon',(I,Admin,Kupon)=>{
	Admin.login()
	Kupon.editCartCoupon('CARTAUTO')
})