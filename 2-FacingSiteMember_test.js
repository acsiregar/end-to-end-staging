Feature('2-(FacingSite) Member');

Scenario('2.1.1 Login with Valid Credentials',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
})
Scenario('2.1.2 Login with Invalid Credentials',(I,Member)=>{
	Member.invalidCredNotif()
})
Scenario('2.2.1 Pick Item to Cart and Checkout',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('nike-air-huarache-','39')
	Member.checkOut()
})
Scenario('2.2.2 Pick Item and Continue Shopping',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('nike-air-huarache-','39')
	Member.continueShopping()
})
Scenario('2.2.3 Pick Item and update quantity',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('onitsuka-tiger---yellow')
	Member.updateQuantity("2")
})
Scenario('2.2.4 Pick Item and update coupon code',(I,Member,Kupon)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('onitsuka-tiger---yellow')
	Kupon.applyCoupon('AUTOTEST')
})
Scenario('2.3.1 Member Place Order',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('onitsuka-tiger---yellow')
	Member.checkOut()
	Member.placeOrder()
})
Scenario('2.3.2 Update Coupon Code on Place Order',(I,Member,Kupon)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('onitsuka-tiger---yellow')
	Member.checkOut()
	Kupon.applyCouponOnPO('AUTOTEST')
	Member.placeOrder()
})
Scenario('2.3.3,.4 Order Shipped to Different Address',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.addItemToCart('onitsuka-tiger---yellow')
	Member.checkOut()
	Member.shippedDifferentAddress()
})
Scenario('2.6.1 Edit account Info',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.editAccountInfo()
})
Scenario('2.6.2 Change Password',(I,Member)=>{
	Member.login('claudiaudy.13@gmail.com','artia123')
	Member.changePassword()
})