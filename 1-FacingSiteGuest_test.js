Feature('1-(FacingSite) Guest');

Scenario('1.2.1 Pick Item to Cart and Checkout',(I,Guest)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.checkOut()
})
Scenario('1.2.2 Pick Item and Continue Shopping',(I,Guest)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.continueShopping()
})
Scenario('1.2.3 Pick Item and Update Quantity',(I,Guest)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.updateQuantity('2')
})
Scenario('1.2.4 Pick Item and Apply Coupon Code',(I,Guest,Kupon)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Kupon.applyCoupon('AUTOTEST')
})
Scenario('1.3.1 Guest Place Order',(I,Guest)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.checkOut()
	Guest.fillFormPO()
	Guest.placeOrder('1')
})
Scenario('1.3.2,.3 Update Coupon Code on Place Order',(I,Guest,Kupon)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.checkOut()
	Guest.fillFormPO()
	Kupon.applyCouponOnPO('AUTOTEST')
	Guest.placeOrder()
})
Scenario('1.3.4,.5 Order Shipped to Different Address',(I,Guest)=>{
	Guest.addItemToCart('nike-tanjun---blue')
	Guest.checkOut()
	Guest.fillFormPO()
	Guest.shippedDifferentAddress()
})