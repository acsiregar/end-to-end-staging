'use strict';

let I,Page;

const _wait_time_short = 5
const _wait_time = 10
const _wait_time_long = 20
const _timeStamp = Date.now()

module.exports = {
	_init() {
    I = actor();
    Page = require('../pages/page.js');
  },
  addItemToCart(link,size='0'){
    I.amOnPage('/products/'+link)
    I.seeInCurrentUrl('/products/'+link)
    Page.check404()
    if(size!='0'){
      I.selectOptin('option_option1',size)
    }
    I.click("//input[@value='Add to cart']")
    I.wait(_wait_time_short)
    I.amOnPage('/cart')
    I.seeInCurrentUrl('cart')
    I.seeElement("//a[contains(@href,'"+link+"')]")
    Page.check404()
    I.saveScreenshot('addedTocart.png')
  },
  checkOut(){
    I.amOnPage('/cart')
    Page.check404()
    I.click("//div[@class='text-right padding-sm']//a[@class='btn-flat'][contains(text(),'Checkout')]")
    I.waitForElement("//input[@id='input_username']",_wait_time)
    I.amOnPage('/account/login')
    Page.check404()
    I.scrollPageToBottom()
    // I.moveCursorTo("//a[@class='btn-flat inline']")
    // I.click("//a[@class='btn-flat inline']")
    // I.wait(_wait_time)
    // I.amOnPage('/cart/place_order/guest')
    I.saveScreenshot('guestCheckout.png')
  },
  fillFormPO(){
    I.amOnPage('/cart/place_order/guest')
    Page.check404()
    I.fillField("//input[@id='input_first_name']","Artia")
    I.fillField("//input[@id='input_phone']","082160956625")
    I.fillField("//input[@id='input_address_line1']","Jalan Peta Barat RT.4/RW.7, RT.4/RW.18, Pegadungan")
    I.selectOption("//select[@name='country']","Indonesia")
    I.fillField("//input[@id='input_state']","DKI Jakarta")
    I.fillField("//input[@id='input_city']","Kota Jakarta Barat - Kalideres")
    I.fillField("//input[@id='input_postal_code']","11840")
    I.fillField("//input[@id='input_email']","claudiaudy.13@gmail.com")
    //I.saveScreenshot('formFilled.png')
  },
  placeOrder(option){
    if(option=='1'){
      I.click("//div[@id='cart-place-order']")
    } else if(option=='2'){
      I.click("//div[@id='cc-dialog-overlay']")
    }
    I.checkOption("//input[@name='agreement']")
    I.moveCursorTo("//input[@value='Place Order']")
    I.waitForEnabled("//input[@value='Place Order']",_wait_time_long)
    I.click("//input[@value='Place Order']")
    I.wait(_wait_time_long)
    Page.check404()
    I.saveScreenshot('placeOrder.png')
  },
  continueShopping(){
    I.click("//a[@class='continue_shop upp']")
    I.wait(_wait_time_short)
    I.amOnPage('/products')
    I.seeInCurrentUrl('/products')
  },
  updateQuantity(quantity){
    I.fillField("//input[@name='quantity']",quantity)
    I.click("//form[@class='cart-update-quantity']//input[@value='Update']")
    I.wait(_wait_time)
    I.seeElement("//input[@value='"+quantity+"']")
  },
  shippedDifferentAddress(){
    // I.amOnPage('/cart/place_order/guest')
    // Page.check404()
    I.wait(_wait_time_short)
    I.checkOption("//input[@name='is_set_delivery']")
    I.waitForEnabled("//input[@id='input_delivery_first_name']",_wait_time_short)
    I.fillField("//input[@id='input_delivery_first_name']", "Claudia")
    I.fillField("//input[@id='input_delivery_phone']","082160956625")
    I.fillField("//input[@id='input_delivery_address_line1']","Jl. Gading Serpong Boulevard No.9, Curug Sangereng, Klp. Dua, Tangerang, Banten 15810")
    I.selectOption("//select[@name='delivery_country']","Indonesia")
    I.fillField("//input[@id='input_delivery_state']","Banten")
    I.fillField("//input[@id='input_delivery_city']","Kota Tangerang Selatan - Serpong")
    I.fillField("//input[@id='input_delivery_postal_code']","15810")
    I.fillField("//input[@id='input_delivery_email']","harujoox@gmail.com")
    I.waitForText("Shipping and Handling",_wait_time_long)
    I.saveScreenshot('shippedtodifferentaddress.png')
  },
  register(){
    I.amOnPage('/')
    I.wait(_wait_time_short)
    I.click("//button[contains(text(),'×')]")
    I.wait(_wait_time_short)
    I.click("//a[@class='last-link']")
    I.wait(_wait_time_short)
    I.scrollPageToBottom()
    I.switchTo("/html/body/div[2]")
    // I.checkOption("recaptcha-accessible-status")
    // I.saveScreenshot('captcha.png')
  },
}