'use strict';

let I, Page;

const _wait_time_short = 5
const _wait_time = 10
const _wait_time_long = 20
const _timeStamp = Date.now()

module.exports = {
	_init() {
    I = actor();
    Page = require('../pages/page.js');
  },
  addProductCoupon(){
		I.amOnPage('/admin/content/coupons')
		I.seeInCurrentUrl('/admin/content/coupons')
		Page.check404()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Product Coupon')]")
		I.seeInCurrentUrl('/admin/content/coupons/new')
		Page.check404()
		//fill field
	},
	addCartCoupon(){
		I.amOnPage('/admin/content/coupons')
		I.seeInCurrentUrl('/admin/content/coupons')
		Page.check404()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Cart Coupon')]")
		I.seeInCurrentUrl('/admin/content/coupons/new/cart')
		Page.check404()
		//fill field
	},
	couponCode(code,title){
		I.fillField('coupon_code',code)
		I.fillField('title',title)
	},
	isAutoApply(){
		I.click("//input[@name='is_auto']") //locating the auto apply checkbox
	},
	setDiscountRate(rate){
		I.selectOption("//select[@name='discount_type']","Percentage")
		I.fillField("//input[@name='discount_rate']",rate)
		I.fillField("//input[@name='maximum_discount_percentage_amount']","10000")
	},
	setDiscountAmount(amount){
		I.selectOption("//select[@name='discount_type']","Absolute")
		I.wait(_wait_time_short)
		I.fillField("//input[@name='discount_amount']",amount)
	},
	setExpiryDate(){
		I.fillField("//input[@name='expiry_date']") 
	},
	isValidForAllProducts(){
		I.checkOption("//input[@name='is_all_products']")
	},
	isValidForProduct(){
		I.checkOption("//option[@value='50']")
	},
	isValidForCategory(){
		I.click("//option[@value='12']")
	},
	isForMemberOnly(){
		I.click("//input[@name='is_member_only']")
		//you can specified the type of the member too
	},
	setMinimumPurchase(){
		I.fillField("//input[@name='minimum_purchase']","1000000")
	},
	applyCoupon(code){
		I.fillField("//input[@placeholder='Coupon Code']",code)
		I.click("//form[@class='cart-coupon']//input[@value='Update']")
		I.saveScreenshot('kuponapplied.png')
	},
	applyCouponOnPO(code){
		I.fillField("//input[@name='coupon_code']",code)
		I.click("//input[@value='Update']")
		I.wait(_wait_time_short)
		//I.click("//span[contains(text(),'Close')]")
		I.saveScreenshot('kuponPO.png')
	},
	saveCoupon(){
		I.click("//div[@class='span9-sirclo btn-div']//div//input[@value='Save']")
	},
	setShippingDiscount(rate){
		I.selectOption("//select[@name='shipping_discount_type']","Percentage")
		I.fillField("//input[@name='discount_rate_shipping']",rate)
	},
	setShippingDiscAmount(amount){
		I.selectOption("//select[@name='shipping_discount_type']","Absolute")
		I.fillField("//input[@name='discount_amount_shipping']",amount)
	},
	editProductCoupon(code){
		I.amOnPage('/admin/content/coupons')
		I.seeInCurrentUrl('/admin/content/coupons')
		Page.check404()
		I.click("//a[contains(text(),'AUTOTEST')]")
		//make coupon auto apply
		this.isAutoApply()
		this.setDiscountAmount('20000')
		this.isForMemberOnly()
		this.saveCoupon()
		I.saveScreenshot('kuponedit.png')
	},
	editCartCoupon(code){
		I.amOnPage('/admin/content/coupons')
		I.seeInCurrentUrl('/admin/content/coupons')
		Page.check404()
		I.click("//a[contains(text(),'AUTOTEST')]")
		//make coupon auto apply
		this.isAutoApply()
		this.setDiscountAmount('20000')
		this.isForMemberOnly()
		this.saveCoupon()
		I.saveScreenshot('kuponedit2.png')
	},
}