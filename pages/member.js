'use strict';

let I,Page;

const _wait_time_short = 5
const _wait_time = 10
const _wait_time_long = 20
const _timeStamp = Date.now()

module.exports = {
	 _init() {
    I = actor();
    Page = require('../pages/page.js');
  },
  //register masih harus dihandle manual karna belum bisa ngecover captcha
  login(username,password){
  	I.amOnPage('/')
  	I.amOnPage('/account/login')
  	I.seeInCurrentUrl('/account/login')
  	Page.check404()
  	I.fillField('username',username)
  	I.fillField('password',password)
  	I.click('Login','#form-row-')
  	I.seeInCurrentUrl('/account')
  	Page.check404()
  },
  logout(){
  	I.amOnPage('/account')
  	I.click('Logout', '#account-sidebar')
  	I.wait(_wait_time)
  	I.seeInCurrentUrl('/account/login')
  	Page.check404()
  },
  invalidCredNotif(){
  	this.login('claudiaudy.13@gmail.com','hahaha')
    I.see('Invalid username or password.')
  },
  addItemToCart(link,size='0'){
    I.amOnPage('/products/'+link)
    I.seeInCurrentUrl('/products/'+link)
    Page.check404()
    if(size!='0'){
      I.selectOption('option_option1',size)
    }
    I.click("//input[@value='Add to cart']")
    I.wait(_wait_time_short)
    I.amOnPage('/cart')
    I.seeInCurrentUrl('cart')
    I.seeElement("//a[contains(@href,'"+link+"')]")
    Page.check404()
  },
  checkOut(){
    I.amOnPage('/cart')
    Page.check404()
    I.click("//div[@class='text-right padding-sm']//a[@class='btn-flat'][contains(text(),'Checkout')]")
    I.wait(_wait_time)
    I.amOnPage('/cart/place_order')
    Page.check404()
  },
  placeOrder(){
    I.amOnPage('/cart/place_order')
    Page.check404()
    I.checkOption("//input[@name='agreement']")
    I.moveCursorTo("//input[@value='Place Order']")
    I.waitForEnabled("//input[@value='Place Order']",_wait_time_long)
    I.click("//input[@value='Place Order']")
    I.wait(_wait_time_long)
    Page.check404()
    I.saveScreenshot('memberPlaceOrder.png')
  },
  continueShopping(){
    I.click("//a[@class='continue_shop upp']")
    I.wait(_wait_time_short)
    I.amOnPage('/products')
    I.seeInCurrentUrl('/products')
    I.saveScreenshot('continueshop.png')
  },
  updateQuantity(quantity){
    I.seeElement("//input[@name='quantity']")
    I.fillField("//input[@name='quantity']",quantity)
    I.click("//form[@class='cart-update-quantity']//input[@value='Update']")
    I.wait(_wait_time)
    I.saveScreenshot("updatequantity.png")
  },
  shippedDifferentAddress(){
    // I.amOnPage('/cart/place_order')
    // Page.check404()
    I.wait(_wait_time_short)
    I.checkOption("//input[@name='is_set_delivery']")
    I.fillField("//input[@id='input_delivery_first_name']", "Claudia")
    I.fillField("//input[@id='input_delivery_phone']","082160956625")
    I.fillField("//input[@id='input_delivery_address_line1']","Jl. Gading Serpong Boulevard No.9, Curug Sangereng, Klp. Dua, Tangerang, Banten 15810")
    I.selectOption("//select[@name='delivery_country']","Indonesia")
    I.fillField("//input[@id='input_delivery_state']","Banten")
    I.fillField("//input[@id='input_delivery_city']","Kota Tangerang Selatan - Serpong")
    I.fillField("//input[@id='input_delivery_postal_code']","15810")
    I.fillField("//input[@id='input_delivery_email']","harujoox@gmail.com")
    I.waitForEnabled("//input[@value='Place Order']",_wait_time_long)
    I.saveScreenshot('shippedtodifferentaddress.png')
  },
  editAccountInfo(){
    //I.amOnPage('/account')
    I.wait(_wait_time_short)
    I.click("//a[@class='btn']")
    I.clearField("//input[@id='input_first_name']")
    I.fillField("//input[@id='input_first_name']","Claudia-"+_timeStamp)
    I.click("//input[@value='Update']")
    I.saveScreenshot('infoUpdated.png')
  },
  changePassword(){
    I.wait(_wait_time_short)
    I.click("//div[@id='account-sidebar']//a[contains(text(),'Change Password')]")
    I.fillField("//input[@id='input_old_password']","artia123")
    I.fillField("//input[@id='input_new_password']","artia123")
    I.fillField("//input[@id='input_confirm_new_password']","artia123")
  },
}