'use strict';

let I, Page;

const _wait_time_short = 5
const _wait_time = 10
const _wait_time_long = 20
const _timeStamp = Date.now()

module.exports = {
	_init(){
		I = actor();
		Page = require('../pages/page.js');
	},

	// SHARED FUNCTIONS
	login(){
		I.amOnPage('/admin')
		I.amOnPage('/admin/login')
		I.seeInCurrentUrl('/admin/login')
		Page.check404()
		I.fillField('username', "artia@sirclo.co.id")
		I.fillField('password', "artia123")
		I.seeElement('input[type=submit]')
		I.click('Login','#login')
		I.seeInCurrentUrl('/admin')
		Page.check404()
	},
	logout(){
		I.amOnPage('/admin')
		I.click('.dropdown')
		I.click('#admin-logout-link')
		I.wait(_wait_time)
		I.seeInCurrentUrl('/admin/logout')
		Page.check404()
	},
	search(option){
		if(option=='1'){ //search category 
			I.amOnPage('/admin/content/categories')
			I.fillField("//input[@name='query']","NIKE")
			I.click("//input[@value='Search']")
			I.saveScreenshot('searchCategory.png')
		} else if(option=='2'){ //search product
			I.amOnPage('/admin/content/products')
			I.fillField("//input[@name='query']","Automation")
			I.click("//input[@value='Search']")
			I.saveScreenshot('searchproducts.png')
		} else if(option=='3'){ //search order
			I.amOnPage('/admin/content/orders')
			I.fillField("//input[@name='query']","Artia")
			I.click("//input[@value='Search']")
			I.saveScreenshot('searchOrders.png')
		} else if(option=='4'){ //search coupon
			I.amOnPage('/admin/content/coupons')
			I.fillField("//input[@name='query']","ProductAll")
			I.click("//input[@value='Search']")
			I.saveScreenshot('searchCoupons.png')
		}
	},
	bulkPublished(){
		I.checkOption("//th[@class='admin-manage-select']//input[@type='checkbox']")
		I.click("//a[contains(text(),'Publishing')]")
		I.click("//a[contains(text(),'Unpublish')]")
		I.saveScreenshot('bulkpublished.png')
	},
	uploadImage(){
		// this.openProductPage()
		// I.click("//a[contains(text(),'"+product+"')]")
		// I.wait(_wait_time)
		I.seeElement("//a[@title='Add Image']")
		I.attachFile("//a[@title='Add Image']","images/onitsuka.jpg")
		I.saveScreenshot('fileuploaded.png')
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
	},
	bulkUpload(option){
		if(option=='1'){
			this.openProductPage()
			I.click("//a[contains(@class,'btn btn-flat admin-manage-bulk-upload')]")
			I.attachFile("//input[@name='itemlist']","/files/products.csv")
			I.click("//input[@value='Upload']")
		} else if(option=='2'){
			this.checkOrder()
			I.click("//a[contains(@class,'btn btn-flat admin-manage-bulk-upload')]")
			I.attachFile("//input[@name='itemlist']","/files/orders.csv")
		}
		I.waitForElement("//div[@class='alert alert-success']",_wait_time_long)
		I.saveScreenshot("bulkUpload-"+option+".png")
	},
	
	// CATEGORY
	newCategory(){
		I.amOnPage('/admin')
		I.click("//a[@href='https://gosendd.mysirclo.com.dmmy.me/admin/content/categories']")
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.click("//a[@class='btn btn-flat admin-manage-add btn-success']")
		I.seeInCurrentUrl('/admin/content/categories/new')
		Page.check404()
		I.fillField('title', "Category Auto")
	},
	newCategoryWithImage(){
		I.amOnPage('/admin')
		I.click("//a[@href='https://gosendd.mysirclo.com.dmmy.me/admin/content/categories']")
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.click("//a[@class='btn btn-flat admin-manage-add btn-success']")
		I.seeInCurrentUrl('/admin/content/categories/new')
		Page.check404()
		I.fillField('title', "With Image Category")
		this.uploadImage()
	},
	parentCategory(parent){
		I.selectOption('parent_id',parent)
		I.saveScreenshot('addparentcategory.png')
	},
	specialCategory(special){
		I.checkOption("is_special")
		I.waitForElement("//select[@name='special_method']",_wait_time_short)
		I.selectOption('special_method',special)
		I.saveScreenshot('addspecialcategory.png')
	},
	editCategory(){
		I.amOnPage('/admin/content/categories')
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.click("//a[contains(text(),'Category Auto')]")
		Page.check404()
		I.clearField('title')
		I.fillField('title', "No Image Category")
	},
	editParentCategory(parent){
		I.amOnPage('/admin/content/categories')
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.click("//a[contains(text(),'Aci Category')]")
		Page.check404()
		I.selectOption('parent_id',parent)
		I.saveScreenshot('editparentcategory.png')
	},
	categoryPublish(){
		I.amOnPage('/admin/content/categories')
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.click("//a[contains(text(),'Aci Category')]")
		Page.check404()
		I.click("//input[@name='is_active']")
		I.saveScreenshot('publishcategory.png')
	},
	saveCategory(){
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.waitForElement("//div[@class='alert alert-success']",_wait_time)
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.saveScreenshot('listcategories.png')
	},
	deleteCategory(){
		I.amOnPage('/admin/content/categories')
		I.seeInCurrentUrl('/admin/content/categories')
		Page.check404()
		I.checkOption("//input[@value='16']")
		I.waitForElement("//button[contains(text(),'Delete')]",_wait_time_short)
		I.click("//button[contains(text(),'Delete')]")
		I.seeInPopup('Are you sure you want to remove the selected items?')
		I.acceptPopup()
		I.waitForElement("//div[@class='alert alert-success']",_wait_time_short)
		I.saveScreenshot('deleteCategory.png')
	},
	reorderCategory(){
		I.amOnPage('/admin/content/categories')
		I.click("//a[contains(@class,'btn btn-flat admin-manage-reorder')]")
		I.seeInCurrentUrl("/admin/content/categories/sort")
		I.dragAndDrop("//li[contains(text(),'Brands')]","//li[contains(text(),'Aci Category')]")
		I.saveScreenshot('reordercategory.png')
	},

	// PRODUCTS
	openProductPage(){
		I.amOnPage('/admin')
		I.click("//a[@href='https://gosendd.mysirclo.com.dmmy.me/admin/content/products']")
		I.seeInCurrentUrl('/admin/content/products')
		Page.check404()
	},
	addNormalProduct(category){
		this.openProductPage()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Normal Product')]")
		I.waitForElement("//label[contains(text(),'Title (English)')]",_wait_time_short)
		I.seeInCurrentUrl('/admin/content/products/new')
		Page.check404()
		I.fillField('title', "Baju Normal")
		I.switchTo('#specification_ifr')
		I.fillField('#tinymce', "<p>Test specs</p>")
		I.wait(_wait_time_short)
		I.switchTo()
		this.uploadImage()
		I.seeElement("//select[@name='general_category_id']")
		I.selectOption('general_category_id',category)
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.wait(_wait_time_long)
		I.see('Please fill the required fields.')
		I.saveScreenshot('addNormalProduct.png')
	},
	addNormalProductNoImage(category){
		this.openProductPage()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Normal Product')]")
		I.waitForElement("//label[contains(text(),'Title (English)')]",_wait_time_short)
		I.seeInCurrentUrl('/admin/content/products/new')
		Page.check404()
		I.fillField('title', "Baju Normal")
		I.switchTo('#specification_ifr')
		I.fillField('#tinymce', "<p>Test specs</p>")
		I.wait(_wait_time_short)
		I.switchTo()
		I.seeElement("//select[@name='general_category_id']")
		I.selectOption('general_category_id',category)
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.wait(_wait_time_long)
		I.see('Please fill the required fields.')
		I.saveScreenshot('addNormalProductNoImage.png')
	},
	editNormalProduct(category){
		this.openProductPage()
    	I.click("//a[contains(text(),'Buat Auto')]")
    	I.selectOption('general_category_id',category) //choose category
    	I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
    	I.wait(_wait_time_long)
    	Page.check404()
    	I.saveScreenshot('editproduct.png')
	},
	updateStockNormal(){
		this.openProductPage()
		I.click("//a[contains(text(),'Buat Auto')]")
		I.clearField("//input[@name='stock']")
		I.fillField("//input[@name='stock']", "100")
	},
	checkUncheckOption(option){
		if(option=='1'){ //normal products
			this.openProductPage()
			I.click("//a[contains(text(),'Buat Auto')]")
			Page.check404()		
		} else if(option=='2'){ //aggregator products
			this.openProductPage()
			I.click("//a[contains(text(),'Automation')]")
			Page.check404()
		}
		I.click("//input[@name='is_featured']")
		I.saveScreenshot("featured-"+option+".png")
		I.click("//input[@name='is_new']")
		I.saveScreenshot("isNew-"+option+".png")
		I.click("//input[@name='is_allow_backorder']")
		I.saveScreenshot("backorder-"+option+".png")
		I.click("//input[@name='is_ignore_stock']")
		I.saveScreenshot("ignorestock-"+option+".png")
	},
	deleteProduct(value){
		this.openProductPage()
    	I.checkOption("//input[@value="+ value +"]")
    	I.wait(_wait_time_short)
    	I.click("//button[contains(text(),'Delete')]")
    	I.wait(_wait_time_short)
    	I.seeInPopup('Are you sure to delete the selected variant(s)?')
		I.acceptPopup()
    	I.dontSeeElement("//input[@value="+ value +"]")
	},
	publishProduct(option){
		if(option=='1'){ //normal product
			this.openProductPage()
			I.click("//a[contains(text(),'Buat Auto')]")
			Page.check404()
		} else if(option=='2'){ //aggregator
			this.openProductPage()
			I.click("//a[contains(text(),'Automation')]")
			Page.check404()
		}
		I.click("//input[@name='is_active']")
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.saveScreenshot("publish-"+option+".png")
	},
	putOnSale(option){
		if(option=='1'){ //normal product
			this.openProductPage()
			I.click("//a[contains(text(),'Buat Auto')]")
			Page.check404()
		} else if(option=='2'){ //aggregator products
			this.openProductPage()
			I.click("//a[contains(text(),'Automation')]")
			Page.check404()
		}
		I.clearField("//input[@name='sale_price']")
		I.fillField("//input[@name='sale_price']","800000")
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.saveScreenshot("saleprice-"+option+".png")
	},
	membershipPrice(option,memberType,price){
		if(option=='1'){ //normal product
			this.openProductPage()
			I.click("//a[contains(text(),'Buat Auto')]")
			Page.check404()
		} else if(option=='2'){ //aggregator products
			this.openProductPage()
			I.click("//a[contains(text(),'Automation')]")
			Page.check404()
		}
		//I.switchTo("//div[@class='admin-special-advanced-pricing-rule']")
		I.selectOption('_special_pricing_member_type',memberType)
		I.fillField("//input[@name='_special_pricing_min_quantity']","2")
		I.fillField("//input[@name='_special_pricing_set_price']",price)
		I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
		I.saveScreenshot("membershipprice-"+option+".png")
	},
	addAggregatorProduct(category){
		this.openProductPage()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Aggregator Product')]")
		I.waitForElement("//label[contains(text(),'General Details')]",_wait_time_short)
		I.seeInCurrentUrl('/admin/content/products/new/aggregator')
		Page.check404()
		I.fillField('title', "Baju aggregator")
    	I.switchTo('#specification_ifr')
		I.fillField('#tinymce', "<p>Test specs</p>")
		I.wait(_wait_time_short)
		I.switchTo()
		this.uploadImage()
    	I.selectOption('general_category_id',category) //choose category
    	I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
    	I.wait(_wait_time_long)
    	Page.check404()
    	I.saveScreenshot('addaggregatorproduct.png')
	},
	addAggregatorProductNoImage(category){
		this.openProductPage()
		I.click("//a[@class='btn dropdown-toggle btn-flat admin-manage-add btn-success']")
		I.click("//a[contains(text(),'New Aggregator Product')]")
		I.waitForElement("//label[contains(text(),'General Details')]",_wait_time_short)
		I.seeInCurrentUrl('/admin/content/products/new/aggregator')
		Page.check404()
		I.fillField('title', "Baju aggregator")
    	I.switchTo('#specification_ifr')
		I.fillField('#tinymce', "<p>Test specs</p>")
		I.wait(_wait_time_short)
		I.switchTo()
    	I.selectOption('general_category_id',category) //choose category
    	I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
    	I.wait(_wait_time_long)
    	Page.check404()
    	I.see('Please fill the required fields.')
    	I.saveScreenshot('addaggregatorproduct.png')
	},
	editAggregatorProduct(category){
		this.openProductPage()
    	I.click("//a[contains(text(),'Buat Auto2')]")
    	I.selectOption('general_category_id',category) //choose category
    	I.click("//div[@class='footer-submit-button-inner']//input[@value='Save']")
    	I.wait(_wait_time_long)
    	Page.check404()
    	I.saveScreenshot('editproductagg.png')
	},
	filterProducts(type=''){
		this.openProductPage()
		I.click("//a[contains(text(),'Show Filter »')]")
		I.seeElement("//input[@name='filter_title']")
		if(type == 'normal'){
			I.selectOption("//select[@name='filter_product_type']",type)
		} else if(type == 'aggregator'){
			I.selectOption("//select[@name='filter_product_type']",type)
		} else if(type == 'published'){
			I.selectOption("//select[@name='filter_is_active']",'yes')
		} else if(type == 'unpublished'){
			I.seeElement("//select[@name='filter_is_active']")
			I.selectOption("//select[@name='filter_is_active']",'no')
		}
		I.seeElement("//button[@type='submit']")
		I.click("//button[@type='submit']")
		I.saveScreenshot("filterProducts-"+type+".png")
	},
	reorderProducts(){
		this.openProductPage()
		I.click("//a[contains(@class,'btn btn-flat admin-manage-reorder')]")
		I.seeInCurrentUrl("/admin/content/products/sort")
		I.dragAndDrop("//ul[@id='sortable']//li[2]","//ul[@id='sortable']//li[1]")
		I.click("//input[@id='sort-image-save-btn']")
		I.wait(_wait_time_short)
		I.click("//a[@class='btn']")
		I.saveScreenshot('reorderproducts.png')
	},

	// ORDERS
	checkOrder(){
		I.amOnPage('/admin/content/orders')
		I.seeInCurrentUrl('/admin/content/orders')
		Page.check404()
		I.saveScreenshot('checkOrder.png')
	},
	changeOrderStatusToProcessing(order_id){
		this.checkOrder()
		I.click("//a[contains(text(),'"+order_id+"')]")
		I.waitForElement("//input[@value='Mark as Processing']",_wait_time_short)
		I.click("//input[@value='Mark as Processing']")
		I.waitForElement("//div[@class='alert alert-success']",_wait_time)
		I.seeElement("//div[@class='alert alert-success']")
		I.saveScreenshot('orderprocessed.png')
	},
	changeOrderStatusToShipped(order_id){
		this.checkOrder()
		I.checkOption("//a[contains(text(),'"+order_id+"')]")
		I.waitForElement("//input[@value='Mark as Shipped']",_wait_time_short)
		I.click("//input[@value='Mark as Shipped']")
		I.waitForElement("//div[@class='alert alert-success']",_wait_time)
		I.seeElement("//div[@class='alert alert-success']")
		I.saveScreenshot('ordershipped.png')
	},
	changeOrderStatusToCancelled(order_id){
		this.checkOrder()
		I.click("//input[@value='"+order_id+"']")
		I.saveScreenshot('clickOrderChange.png')
		I.waitForElement("//a[contains(@class,'btn-info')]",_wait_time)
		I.seeElement("//a[contains(@class,'btn-info')]")
		I.click("//a[contains(@class,'btn-info')]")
		I.click("//a[contains(text(),'Cancelled')]")
		I.seeInPopup("Are you sure you want to cancel the selected order(s)?")
		I.acceptPopup()
		I.saveScreenshot('cancelled.png')
	},
	exportOrder(){
		this.checkOrder()
		I.click("//a[contains(@class,'btn btn-flat admin-manage-export')]")
	},
	selectReportOption(option){
		this.checkOrder()
		I.click("//a[contains(@class,'btn dropdown-toggle btn-flat admin-manage-report')]")
		if(option == '1'){
			I.click("//a[contains(text(),'Monthly Sales')]")
			I.waitForText('Report for Monthly Sales Performance')
		} else if(option == '2'){
			I.click("//a[contains(text(),'Bestseller Products')]")
			I.waitForText('Report for Best Selling Products')
		} else if(option == '3'){
			I.click("//a[contains(text(),'Sales per Member')]")
			I.waitForText('Report for Best Member')
		} else if(option == '4'){
			I.click("//a[contains(text(),'Waiting List')]")
			I.waitForText('Report for Waiting List')
		} else if(option == '5'){
			I.click("//a[contains(text(),'Abandoned Carts')]")
			I.waitForText('Report for Abandoned Carts')
		} else if(option == '6'){
			I.click("//a[contains(text(),'Inventory')]")
			I.waitForText('Inventory Report')
		} else if(option == '7'){
			I.click("//a[contains(text(),'Shipping Addresses')]")
			I.waitForText('Report for Shipping Addresses')
		} else if(option == '8'){
			I.click("//a[contains(text(),'Order Product Detail Report')]")
			I.waitForText('Report for Order Product Detail')
		}
	},
	deleteOrder(order_id){
		this.checkOrder()
		I.click("//input[@value='"+order_id+"']")
		I.saveScreenshot('clickOrder.png')
		I.waitForElement("//button[contains(text(),'Delete')]")
		I.seeElement("//button[contains(text(),'Delete')]")
		I.click("//button[contains(text(),'Delete')]")
		I.seeInPopup("Are you sure you want to remove the selected items?")
		I.acceptPopup()
		I.waitForElement("//div[@class='alert alert-success']",_wait_time_short)
		I.saveScreenshot('deleteorder.png')
	},
	filterOrders(type='',value){
		this.checkOrder()
		I.click("//a[contains(text(),'Show Filter »')]")
		I.seeElement("//input[@name='filter_order_id']")
		if(type == 'ID'){
			I.fillField("//input[@name='filter_order_id']",value)
		} else if(type == 'order status'){
			I.selectOption("//select[@name='filter_order_status']",value)
		} else if(type == 'payment method'){
			I.selectOption("//select[@name='filter_payment_method']",value)
		} else if(type == 'shipping method'){
			I.seeElement("//input[@name='filter_shipping_method']")
			I.fillField("//input[@name='filter_shipping_method']",value)
		}
		I.seeElement("//button[@type='submit']")
		I.click("//button[@type='submit']")
		I.saveScreenshot("filterOrders-"+type+".png")
	}
}