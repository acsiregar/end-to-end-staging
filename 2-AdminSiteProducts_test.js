Feature('2-(AdminSite) Products');

Scenario('2.1.1 Add Normal Product',(I,Admin)=>{
	Admin.login()
	Admin.addNormalProduct('NIKE')
})
Scenario('2.1.2 Add Normal Product with No Image', (I,Admin)=>{
	Admin.login()
	Admin.addNormalProductNoImage('NIKE')
})
Scenario('2.1.3 Add Aggregator Product',(I,Admin)=>{
	Admin.login()
	Admin.addAggregatorProduct('Asics Onitsuka')
})
Scenario('2.1.4 Add Aggregator Product with No Image',(I,Admin)=>{
	Admin.login()
	Admin.addAggregatorProductNoImage('NIKE')
})
Scenario('2.2.2 Edit Category Product', (I,Admin)=>{
	Admin.login()
	Admin.editNormalProduct('NIKE')
})
Scenario('2.2.3 Update Stock', (I,Admin)=>{
	Admin.login()
	Admin.updateStockNormal()
})
Scenario('2.2.4,5,6,7 Normal Product Check Uncheck Option', (I,Admin)=>{
	Admin.login()
	Admin.checkUncheckOption('1')
})
Scenario('2.2.8 Publish/Unpublish Normal Product', (I,Admin)=>{
	Admin.login()
	Admin.publishProduct('1')
})
Scenario('2.2.10 Normal Product on Sale', (I,Admin)=>{
	Admin.login()
	Admin.putOnSale('1')
})
Scenario('2.2.11 Normal Membership Special Price', (I,Admin)=>{
	Admin.login()
	Admin.membershipPrice('1','Silver','1000000')
})
Scenario('2.3.2 Edit Category Product', (I,Admin)=>{
	Admin.login()
	Admin.editAggregatorProduct('NIKE')
})
Scenario('2.3.6,.7,.8,.9 Aggregator Check Uncheck Option',(I,Admin)=>{
	Admin.login()
	Admin.checkUncheckOption('2')
})
Scenario('2.3.10 Publish/Unpublish Aggregator Product',(I,Admin)=>{
	Admin.login()
	Admin.publishProduct('2')
})
Scenario('2.3.11 Aggregator Product on Sale', (I,Admin)=>{
	Admin.login()
	Admin.putOnSale('2')
})
Scenario('2.3.12 Aggregator Membership Special Price', (I,Admin)=>{
	Admin.login()
	Admin.membershipPrice('2','Silver','1000000')
})
Scenario('2.4.1 Bulk Upload Products',(I,Admin)=>{
	Admin.login()
	Admin.bulkUpload('1')
})
Scenario('2.6.1 Reorder Products',(I,Admin)=>{
	Admin.login()
	Admin.reorderProducts('2')
})
Scenario('2.8.1 Filter Products',(I,Admin)=>{
	Admin.login()
	Admin.filterProducts('normal')
})
Scenario('2.9 Search Products',(I,Admin)=>{
	Admin.login()
	Admin.search('2')
})